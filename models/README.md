## Models
In this repository also trained models are provided in the `models` directory.
Each of them contains the following files:
```
best_model.out:
    the weights of the best model during training in terms of testing loss

results.csv:
    the training and testing loss during training for each epoch

config.yml:
    configuration parameters for the training process
```
In the `validation` repository, the results for the predictions are stored.
They result from applying the trained models on the validation dataset.
