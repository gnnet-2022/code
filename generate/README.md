# Generate Data

## Generate Topologies
First, use the python script to generate topologies. They are stored in `json` format.
For each topology the number of routers and flows and all parameters are randomized as
specified in the `config.yaml` or using command line arguments.

```
python generate-topology.py --config config.yaml
```

## Setup ns-3 Simulation
We used a default Debian Bullseye image for the simulator and additionally installed
these dependencies before building `ns-3`.
```
apt install libboost-dev nlohmann-json3-dev
```

For simulation, we use [ns-3.35](https://github.com/nsnam/ns-3-dev-git) including some
minor changes. To install ns-3 ith these changes you can follow these instructions.

```
git clone https://github.com/nsnam/ns-3-dev-git.git --branch ns-3.35 --single-branch
cd ns-3-dev-git
git apply file.patch

export CXXFLAGS="-Wno-error=unused-function"
./waf configure --build-profile=optimized --enable-examples
./waf build
```

This should compile ns-3 and the simulation examples.

Afterwards, the simulation of a configuration file can be executed with
```
./waf --run="tcp-flow-measurements --topology-file=../topology.json"
```
which writes the output into a new json file in the `out` directory within
the `ns-3` repository.