#!/bin/python3

from dataclasses import dataclass
import argparse
import heapq
from secrets import SystemRandom
import json
import yaml
import numpy as np
from typing import List

tcp_algos = [
    "ns3::TcpCubic",
    "ns3::TcpBic",
    "ns3::TcpLinuxReno",
    # "ns3::TcpBbr",
    "ns3::TcpVegas",
    "ns3::TcpLedbat",
    "ns3::TcpVeno",
    "ns3::TcpIllinois",

]


@dataclass
class Node:
    id: int
    tcp_congestion_algo: str = "ns3::TcpCubic"
    queue_size: int = 125

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "tcp_congestion_algo": self.tcp_congestion_algo,
            "queue_size": self.queue_size
        }

    def __str__(self):
        return f"<[Node] id={self.id}, tcp_congestion_algo={self.tcp_congestion_algo}, queue_size={self.queue_size}>"


@dataclass
class Link:
    id: int
    lhs: Node
    rhs: Node
    bandwidth: int
    delay: float
    loss: float

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "lhs": self.lhs.id,
            "rhs": self.rhs.id,
            "bandwidth": self.bandwidth,
            "delay": self.delay,
            "loss": self.loss,
        }

    def __str__(self):
        return f"<[Link] id={self.id}, bandwidth={self.bandwidth}Mbit/s," \
               f"delay={self.delay}ms, loss={self.loss}, router_a={self.lhs.id}, router_b={self.rhs.id}>"


@dataclass
class Flow:
    id: int
    host_a: Node
    host_b: Node
    start: float  # starttime
    # end: float  # endttime
    time_limit: float
    data_limit: int = None  # amount to send in bytes

    route: List[int] = None
    tcp_congestion_algo: str = None

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "host_a": self.host_a.id,
            "host_b": self.host_b.id,
            "start": self.start,
            # "end": self.end,
            "time_limit": self.time_limit,
            "data_limit": self.data_limit,
            "route": self.route,
            "tcp_congestion_algo": self.tcp_congestion_algo
        }

    def __str__(self):
        return f"<[Flow] id={self.id}, host_a={self.host_a}, host_b={self.host_b}>"


def create_graphviz(nodes, links) -> str:
    graph = "strict graph links {\n"

    # routers
    graph += f"node [shape=ellipse];"
    for n in nodes:
        graph += f" N{n.id};"
    graph += "\n"

    # ir links
    for l in links:
        graph += f"N{l.lhs.id} -- N{l.rhs.id} [label=\"{l.delay:.3f}ms\"];\n"

    graph += "}"
    return graph


def get_shortest_path(node_a, node_b, node_count, links):
    route = [node_b.id]
    # Djikstra
    start = node_a.id
    pred = np.empty(node_count, dtype=np.uint32)
    dist = np.full(node_count, np.Infinity)
    dist[start] = 0
    queue = []
    for r in range(node_count):
        heapq.heappush(queue, (dist[r], r))

    while len(queue) > 0:
        _, r = heapq.heappop(queue)
        neighbour_links = [l for l in links if l.lhs.id == r or l.rhs.id == r]
        for l in neighbour_links:
            if r == l.lhs.id:
                o = l.rhs.id
            else:
                o = l.lhs.id
            if dist[r] + l.delay < dist[o]:
                dist[o] = dist[r] + l.delay
                heapq.heappush(queue, (dist[o], o))
                pred[o] = r

                if o == node_b.id:
                    # Done
                    break
        else:
            continue

        break

    p = int(pred[node_b.id])
    route.append(p)
    while p != node_a.id:
        p = int(pred[p])
        route.append(p)

    return list(reversed(route))


def create_json_topology_str(nodes: List[Node], links: List, flows: List[Flow], duration: float, pretty: bool) -> str:
    dict = {
        "node_count": len(nodes),
        "nodes": [n.to_dict() for n in nodes],
        "link_count": len(links),
        "links": [l.to_dict() for l in links],
        "flow_count": len(flows),
        "flows": [f.to_dict() for f in flows],
        "sim_duration": duration,
        # "graphviz": create_graphviz(nodes, links)
    }

    if pretty:
        return json.dumps(dict, indent=4)
    else:
        return json.dumps(dict)


def main():
    parser = argparse.ArgumentParser(description="This Script generates a network topology for simulation input")
    parser.add_argument("--routers-min", type=int, default=5, help="Minimum amount of routers in the topology")
    parser.add_argument("--routers-max", type=int, default=12, help="Maximum amount of routers in the topology")
    parser.add_argument("--flows-min", type=int, default=1, help="Minimum amount of flows in the topology")
    parser.add_argument("--flows-max", type=int, default=10, help="Maximum amount of flows in the topology")
    parser.add_argument("--link-bandwidth-min", type=int, default=10, help="Minimum bandwidth of links in Mbits/s")

    parser.add_argument("--link-bandwidth-max", type=int, default=100, help="Maximum bandwidth of links, in Mbits/s")
    parser.add_argument("--link-bandwidth-fixed", type=int, default=None,
                        help="If set, all links are set to have the specified bandwidth")
    parser.add_argument("--link-bandwidth-step", type=int, default=10,
                        help="Maximum step size of bandwidth of links, in Mbits/s")
    parser.add_argument("--queue-size-fixed", type=int, default=None,
                        help="Sets the queue sizes of all interfaces to this value in BDP, if set")
    parser.add_argument("--queue-size-max", type=int, default=5, help="Maximum queue size in BDP")
    parser.add_argument("--queue-size-min", type=int, default=1, help="Minimum queue size in BDP")
    parser.add_argument("--queue-size-step", type=int, default=.5, help="queue size step in BDP")
    parser.add_argument("--link-delay-fixed", type=int, default=None,
                        help="Sets the propagation delay of all links to this value in ms, if set")
    parser.add_argument("--link-delay-max", type=int, default=50, help="Maximum propagation delay of links in ms")
    parser.add_argument("--link-delay-min", type=int, default=5, help="Minimum propagation delay of links in ms")
    parser.add_argument("--link-delay-step", type=int, default=5, help="Minimum propagation delay of links in ms")
    parser.add_argument("--outfile", type=str, default="topology.json", help="File to write the generated topology to")
    parser.add_argument("--pretty", action="store_true", help="Pretty print JSON file")
    parser.add_argument("--sim-runtime", type=float, default=60.0, help="Simulation Runtime")
    parser.add_argument("--config", type=str, help="configuration YAML file which overwrites all args.")

    args = parser.parse_args()

    if args.config:
        with open(args.config, 'r') as stream:
            data_loaded = yaml.safe_load(stream)
            for k, v in data_loaded.items():
                try:
                    if k not in args:
                        raise ValueError(f'Unknown parameter {k}')
                    setattr(args, k, v)
                except Exception as e:
                    print(e)

    sim_start = 0.0
    sim_end = args.sim_runtime  # 60s default runtime

    # Generate Routers
    # random.seed()
    random = SystemRandom()
    router_count = random.randint(args.routers_min, args.routers_max)

    def get_random_queue_size(rate, delay):
        """
            rate in mbit/s
            delay in ms
            result: queue size in KB
        """
        bdp = rate * delay / 8  # in KB

        if args.queue_size_fixed is not None:
            queue_size = args.queue_size_fixed
        else:
            queue_size = random.choice(
                np.arange(
                    args.queue_size_min,
                    args.queue_size_max,
                    args.queue_size_step
                )
            )
        return int(queue_size * bdp)

    def get_random_delay():
        if args.link_delay_fixed is not None:
            return args.link_delay_fixed
        else:
            return random.choice([x for x in range(args.link_delay_min, args.link_delay_max + 1, args.link_delay_step)])

    def get_random_bandwidth():
        if args.link_bandwidth_fixed is not None:
            return args.link_bandwidth_fixed
        else:
            return random.choice([x for x in range(args.link_bandwidth_min, args.link_bandwidth_max + 1,
                                                   args.link_bandwidth_step)])

    routers = []
    for i in range(router_count):
        r = Node(i, tcp_congestion_algo=random.choice(tcp_algos), queue_size=0)
        # Queue sizes are set later
        routers.append(r)

    # Setup links between routers
    links = []

    if router_count == 1:
        pass

    else:
        # First, create random tree so that all routers are connected and reachable from any other router
        # https://stackoverflow.com/questions/14878228/creating-a-random-tree
        # Random i sequence as prüfer code
        p = [random.randrange(0, router_count) for _ in range(router_count - 2)]
        b = set(range(0, router_count))

        for i in range(router_count - 2):
            u = p[0]
            v = min(b - set(p))
            p.pop(0)
            b.remove(v)

            links.append(Link(i, routers[u], routers[v], get_random_bandwidth(), get_random_delay(), 0))

        links.append(Link(router_count - 1, routers[b.pop()], routers[b.pop()], get_random_bandwidth(),
                          get_random_delay(), 0))

    # Generate Flows and add hosts
    flow_count = random.randint(
        args.flows_min,
        args.flows_max,
        # min(args.flows_max, max(np.floor(router_count / 2.0) - 1, 1))
    )
    flows = []
    hosts = []

    for i in range(flow_count):

        if len(routers) == 1:
            ra = routers[0]
            rb = routers[0]
        else:
            ra = random.choice([r for r in routers])
            rb = random.choice([r for r in routers if r.id != ra.id])

        tcp_algo = random.choice(tcp_algos)

        ha_rate = get_random_bandwidth()
        ha_delay = get_random_delay()

        hb_rate = get_random_bandwidth()
        hb_delay = get_random_delay()

        ha = Node(
            len(routers) + len(hosts),
            tcp_congestion_algo=tcp_algo,
            queue_size=get_random_queue_size(ha_rate, ha_delay)
        )
        hosts.append(ha)
        links.append(Link(len(links), ra, ha, ha_rate, ha_delay, 0))

        hb = Node(
            len(routers) + len(hosts),
            tcp_congestion_algo=tcp_algo,
            queue_size=get_random_queue_size(hb_rate, hb_delay)
        )
        hosts.append(hb)
        links.append(Link(len(links), rb, hb, hb_rate, hb_delay, 0))

        f = Flow(
            len(flows),
            ha,
            hb,
            sim_start,
            sim_end
        )
        flows.append(f)

    # Remove dead routers and links
    visited = set()
    for f in flows:
        route = get_shortest_path(f.host_a, f.host_b, len(routers) + len(hosts), links)
        for e in route:
            visited.add(e)

    routers = [r for r in routers if r.id in visited]
    links = [l for l in links if l.lhs.id in visited and l.rhs.id in visited]

    # fix node ids
    for i, r in enumerate(routers):
        r.id = i

    for i, h in enumerate(hosts):
        h.id = len(routers) + i

    for i, l in enumerate(links):
        l.id = i

    for f in flows:
        f.route = get_shortest_path(f.host_a, f.host_b, len(routers) + len(hosts), links)
        f.tcp_congestion_algo = f.host_a.tcp_congestion_algo

    # Set queue sizes for routers using the minimum BDP of all connected links
    for node in routers:
        node_links = [l for l in links if node.id in [l.lhs.id, l.rhs.id]]
        node_links.sort(key=lambda x: x.bandwidth * x.delay)
        node.queue_size = get_random_queue_size(node_links[0].bandwidth, node_links[0].delay)

    # write file
    json_topo = create_json_topology_str(routers + hosts, links, flows, sim_end, pretty=args.pretty)
    outfile_name = args.outfile
    with open(outfile_name, "w") as fd:
        fd.write(json_topo)


if __name__ == '__main__':
    main()
