# Preprocess Data

In this repository, only the raw simulation results (in `json` format)
are available.
In the `data` directory, first decompress the files with
```
tar -zxvf results.tar.gz
```
This should create a `results` directory which contains 100K and 10K samples.

## Normalization
We applied a simple min-max normalization to the data scaling all values to the
range [0, 1].
To get the required min and max values of a dataset, you can use the following function:
```
python minmax.py --in ../data/dataset-train/results --out ../data/dataset-train/minmax.json
```
This creates the `minmax.json` file which is later used for normalization.

## Preprocess Data
The raw data in `json` format is converted in the matrix format used for the
GNN as explained in the following.

```
python preprocess.py --dataset ../data/dataset-train
python preprocess.py --dataset ../data/dataset-validate
```

This command uses results in the `results` directory and the `minmax.json` file.
By default, data is preprocessed for all graph representations but if specified with
`--parser <Parser names>` only a subset of processed files can be generated.
The output is writen into the `<dataset>/processed` directory.

## Example
You can also use a simple dataset which only contains one file using the following command:
```
python3 preprocess.py --parser RTTAckPathParser --dataset test
```