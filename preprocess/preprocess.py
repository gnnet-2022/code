#!/usr/bin/env python3

import glob
import os
import tqdm

import json
import argparse
import numpy as np

from parser.rtt import RTTParser
from parser.rtt_ack import RTTAckParser
from parser.rtt_ack_path import RTTAckPathParser
from parser.rate import RateParser
from parser.rate_ack import RateAckParser
from parser.rate_ack_path import RateAckPathParser
from parser.queue import QueueParser
from parser.queue_ack import QueueAckParser
from parser.queue_ack_path import QueueAckPathParser
from parser.queue_latency import QueueLatencyParser
from parser.queue_latency_ack import QueueLatencyAckParser
from parser.queue_latency_ack_path import QueueLatencyAckPathParser

PARSER = [
    RTTParser,
    RTTAckParser,
    RTTAckPathParser,
    RateParser,
    RateAckParser,
    RateAckPathParser,
    QueueParser,
    QueueAckParser,
    QueueAckPathParser,
    QueueLatencyParser,
    QueueLatencyAckParser,
    QueueLatencyAckPathParser,
]

MODELS = {
    k.__name__: k for k in PARSER
}


def minmax_normalization(minmax_data, parameter_name):
    """provides a minmax normalization function for the given parametername and minmax json"""

    def f_minmax(x):
        if (minmax_data[parameter_name]["max"] - minmax_data[parameter_name]["min"]) == 0:
            return 1.0

        return (x - minmax_data[parameter_name]["min"]) / (minmax_data[parameter_name]["max"] - minmax_data[parameter_name]["min"])

    return f_minmax


def get_minmax(minmax_file):
    try:
        with open(minmax_file, 'r') as f:
            minmax_data = json.load(f)

        def f_minmax(x):
            return minmax_normalization(minmax_data, x)
    except:
        def f_minmax(x):
            return lambda: x
    return f_minmax


def process(input, parser):

    output = []
    for result in tqdm.tqdm(glob.glob(os.path.join(input, '*.json'))):
        with open(result, 'r') as f:
            data = json.load(f)
            processed = parser.import_raw(data)
            matrix = parser.graph2matrix(processed, lambda x: x)
            matrix.file = os.path.basename(result)
            output.append(matrix)
    return output


def main():
    args = parse_args()

    result_dir = os.path.join(args.dataset, 'results')
    f_minmax = get_minmax(os.path.join(args.dataset, 'minmax.json'))

    parser = args.parser or MODELS.keys()

    for name in parser:
        print(f'Processing {name}')
        parser = MODELS[name](f_minmax)
        output = process(result_dir, parser)
        output_dir = os.path.join(args.dataset, 'processed', f'{name.lower()}')
        os.makedirs(output_dir, exist_ok=True)
        output_file = os.path.join(output_dir, 'processed.npz')
        parser.export_data(np.asarray(output, dtype=object), output_file)


def parse_args():
    parser = argparse.ArgumentParser(description="This script parses the simulation results and exports them for training the neural network")
    parser.add_argument("--dataset", type=str, help="Path to a dataset directory. All preprocessing steps are done.", required=True)
    parser.add_argument("--parser", nargs='*', choices=list(MODELS.keys()), help="Used parser")
    return parser.parse_args()


if __name__ == "__main__":
    main()
