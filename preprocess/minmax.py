import argparse
import os
import json
from pprint import pprint
import numpy as np
import pandas as pd
import tqdm
import glob

from parser import get_path_links

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="This script parses the simulation results and exports the minimum and maximum values for normalization")
    parser.add_argument("--in", nargs="+", dest="input", type=str, help="The simulation result folder(s) to analyze", required=True)
    parser.add_argument("--out", type=str, help="The output filename (default = minmax.json)", default="minmax.json")
    parser.add_argument("--csv", type=str, help="Write raw data to csv")
    args = parser.parse_args()

    retval = {
        "flow_rate": {},
        "flow_rtt": {},
        "link_rate": {},
        "link_delay": {},
        "queue_size": {},
        "queue_len": {},
        "queue_util": {},
        "path_order": {},
        "queue_latency": {}
    }

    df = []

    files = sum([glob.glob(os.path.join(input, '*.json')) for input in args.input], [])

    for file in tqdm.tqdm(files, total=len(files)):

        with open(file, "r") as fd:
            data = json.loads(fd.read())

        flows = data["input"]["flows"]
        links = data["input"]["links"]
        nodes = data["input"]["nodes"]
        flow_results = data["flow_results"]

        for l in links:
            df.append({
                'link_rate': l['bandwidth'],
                'link_delay': l['delay']
            })

        for n in nodes:
            df.append({
                'queue_size': n['queue_size'],
            })

        for f in flows:
            flow_id = f['id']
            flow_path = f["route"]

            path_links = get_path_links(links, flow_path)
            bw = min([l['bandwidth'] for l in path_links])
            rtt = 2 * sum([l['delay'] for l in path_links])

            df.append({
                'flow_rate': flow_results[flow_id]['mean_bandwidth'],
                'flow_rtt': flow_results[flow_id]['mean_rtt'],
                'queue_latency': flow_results[flow_id]['mean_rtt'] - rtt,
            })

            df.append({
                'path_order': len(flow_path),
            })

            for i, (prev, current) in enumerate(zip(flow_path, flow_path[1:])):
                for l in data["input"]["links"]:
                    if  l["lhs"] == prev and l["rhs"] == current:
                        # match
                        queue_res = data["queue_results"][l["id"]]
                        if queue_res["link"] != l["id"]:
                            raise RuntimeError("id mismatch")
                        df.append({
                            'queue_len': queue_res["lhs"],
                            'queue_util': queue_res["lhs_util"],
                        })
                        break
                    elif l["lhs"] == current and l["rhs"] == prev:
                        queue_res = data["queue_results"][l["id"]]
                        if queue_res["link"] != l["id"]:
                            raise RuntimeError("id mismatch")
                        df.append({
                            'queue_len': queue_res["rhs"],
                            'queue_util': queue_res["rhs_util"],
                        })
                        break
                else:
                    raise RuntimeError("no link found")
    df = pd.DataFrame(df)

    if args.csv:
        df.to_csv(args.csv, index=False)

    for k, v in retval.items():
        v['min'] = df[k].min()
        v['max'] = df[k].max()

    pprint(retval)
    with open(args.out, "w") as out:
        json.dump(retval, out, indent=2)