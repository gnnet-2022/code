#!/usr/bin/env python3

# Parser for Flow Rate prediction
# This parser adds:
# * interface nodes for each interface
# * flow nodes connected to the interface nodes directly

import enum
import networkx as nx
from typing import Callable
from . import TcpAlgos, flow_label, ack_label, path_label, interface_label, tcp_algo_map, find_obj

from . import Parser


class NodeType(enum.IntEnum):
    Interface = 1
    Flow = 2
    Ack = 3


class QueueAckParser(Parser):

    def __init__(self, minmax: Callable[[str], Callable[[float], float]]):
        node_parameters = {
            "ntype": {"encoding": len(NodeType) + 1, "is_y": False},
            "link_rate": {"encoding": 0, "is_y": False, "normalization": minmax("link_rate")},
            "link_delay": {"encoding": 0, "is_y": False, "normalization": minmax("link_delay")},
            "queue_size": {"encoding": 0, "is_y": False, "normalization": minmax("queue_size")},
            "queue_util": {"encoding": 0, "is_y": True, "mask": [NodeType.Interface], "normalization": minmax("queue_util")},
            "flow_tcp_algo": {"encoding": len(TcpAlgos) + 1, "is_y": False},
        }

        Parser.__init__(self, NodeType, node_parameters)
        self.minmax = minmax
        self.node_types = NodeType


    def process_data(self, data):

        G = nx.Graph()
        parameter_template = {k: float('nan') for k in self.node_parameters}

        def get_params(kwargs):
            params = dict(parameter_template)
            params.update((k, kwargs[k]) for k in set(kwargs).intersection(params))
            return params

        input_data = data["input"]

        # Iterate over all flows
        for f in input_data["flows"]:
            # Add flow node
            flow_results = data["flow_results"][f["id"]]
            tcp_algo = input_data["nodes"][f["host_a"]]["tcp_congestion_algo"]
            G.add_node(flow_label(f["id"]), **get_params({
                "ntype": self.node_types.Flow,
                "flow_tcp_algo": tcp_algo_map(tcp_algo),
            }))

            G.add_node(ack_label(f["id"]), **get_params({
                "ntype": self.node_types.Ack,
            }))

            G.add_edge(flow_label(f["id"]), ack_label(f["id"]))

            def add_path(path, f_label):
                for _, (prev, current) in enumerate(zip(path, path[1:])):
                    # check if interface node already exists
                    if_label = interface_label(prev, current)
                    if if_label not in G:
                        for l in input_data["links"]:
                            # Find corresponding link
                            if l["lhs"] == prev and l["rhs"] == current:
                                l_delay = l["delay"]
                                l_bandwidth = l["bandwidth"]
                                l_queue_utilization = find_obj(data['queue_results'], l['id'], key='link')["lhs_util"]
                                break
                            elif l["lhs"] == current and l["rhs"] == prev:
                                l_delay = l["delay"]
                                l_bandwidth = l["bandwidth"]
                                l_queue_utilization = find_obj(data['queue_results'], l['id'], key='link')["rhs_util"]
                                break
                        else:
                            raise RuntimeError(f"No link found for interface label {if_label}")

                        queue_size = input_data["nodes"][prev]["queue_size"]
                        # Add interface node
                        G.add_node(if_label, **get_params({
                            "ntype": self.node_types.Interface,
                            "link_rate": l_bandwidth,
                            "link_delay": l_delay,
                            "queue_size": queue_size,
                            "queue_util": l_queue_utilization,
                        }))

                    G.add_edge(if_label, f_label)

            # Add path and links
            flow_path = f["route"]
            add_path(flow_path, flow_label(f["id"]))
            add_path(flow_path[::-1], ack_label(f["id"]))

        return G
