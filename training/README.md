## Training
After preprocessing the data, models can be trained.
For example with the following command:
```
python train.py --dataset ../data/dataset-train/processed/rttackpathparser/processed.npz
```

You already find trained models using the training dataset in the `models` directory.
There you also find the used hyperparameters for training.
