#!/usr/bin/env python3
"""
Copyright (c) 2019 Fabien Geyer
Copyright (c) 2021 Benedikt Jaeger
Copyright (c) 2021 Max Helm

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os
import git
import yaml
import csv
import argparse
import numpy as np
from tqdm import tqdm
import datetime
import uuid

import torch
import torch.nn as nn
from torch.utils.data import random_split
import torch_geometric.nn as gnn
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader


class GGNN(nn.Module):
    def __init__(self, num_features, num_classes, hidden_size, linear_layer_input, dropout, num_layers, nunroll,
                 dropout_gru, **kwargs):
        super().__init__()
        self.num_features = num_features
        self.num_classes = num_classes
        self.linear_layer_input = linear_layer_input
        self.hidden_size = hidden_size
        self.dropout = dropout
        self.cell_input = self.hidden_size if self.linear_layer_input else self.num_features
        self.num_layers = num_layers
        self.nunroll = nunroll
        self.dropout_gru = dropout_gru
        self.cell = nn.ModuleList()
        self.after_cell = nn.ModuleList()

        for i in range(self.num_layers):
            self.cell.append(gnn.GatedGraphConv(self.cell_input, nunroll))
            linear_tmp = nn.Sequential(*[
                nn.LayerNorm(self.cell_input, elementwise_affine=True),
                nn.LeakyReLU(inplace=True),
                nn.Dropout(dropout_gru)
            ])
            self.after_cell.append(linear_tmp)

        # First layers
        self.fci = nn.Sequential(*[
            nn.Linear(self.num_features, hidden_size),
            nn.LeakyReLU(),
            nn.Dropout(self.dropout)
        ])

        # Last layers
        self.fco = nn.Sequential(*[
            nn.Linear(self.cell_input, self.hidden_size),
            nn.LeakyReLU(),
            nn.Dropout(self.dropout),
            nn.Linear(hidden_size, self.num_classes),
            nn.Sigmoid()
        ])

    def forward(self, data):
        x = data.x.float()

        if self.linear_layer_input:
            x = self.fci(x)

        for cell_idx, cell_elem in enumerate(self.cell):
            x = cell_elem(x, data.edge_index.long())
            x = self.after_cell[cell_idx](x)

        x = self.fco(x)
        return x


def import_npz(dataset_path):
    data = np.load(dataset_path, allow_pickle=True)
    res = []
    num_edges = []
    num_nodes = []
    for data_point in tqdm(data, total=len(data), leave=False, ncols=0):
        data_dict = {}
        for elem in data[data_point]:
            try:
                if elem[0] == 'edge_index':
                    num_edges.append(2 * int(elem[1].shape[1]))
                if elem[0] == 'num_nodes':
                    num_nodes.append(int(elem[1]))
                data_dict[elem[0]] = torch.from_numpy(elem[1])
            except Exception as e:
                data_dict[elem[0]] = elem[1]
        data_tmp = Data.from_dict(data_dict)
        res.append(data_tmp)
        data_dict = None
    return res


def data_loader(dataset, train_test_split, batch_size):
    dataset_size = len(dataset)
    train_size = int(dataset_size * train_test_split)
    test_size = dataset_size - train_size

    dataset_train, dataset_test = random_split(dataset, [train_size, test_size])

    loader_train = DataLoader(dataset_train, batch_size=batch_size, shuffle=True)
    loader_test = DataLoader(dataset_test, batch_size=batch_size, shuffle=True)
    print(f"Dataset size {dataset_size}: split to train={train_size} test={test_size}")
    return loader_train, loader_test


def train_model(model, dataset, criterion, args):
    train_test_split = args.train_test_split
    batch_size = args.batch_size
    learning_rate = args.learning_rate
    model_output_dir = args.model_output_dir
    best_models = {}

    loader_train, loader_eval = data_loader(dataset, train_test_split, batch_size)

    device = torch.device(args.device)

    model = model.to(device)
    torch.cuda.manual_seed(args.seed)

    # Adam optimizer with decaying learning rate
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=args.weight_decay)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=args.lr_scheduler_factor)

    # Main loop
    training_results = []
    best_val_loss = float('inf')

    for epoch in range(args.epochs):

        precision_score_eval = None
        recall_score_eval = None
        losses_train = []
        losses_eval = []

        # Train model on training data
        model.train()

        pbar = tqdm(total=len(loader_train), desc='Epoch {:3d}'.format(epoch), ncols=0)

        for data in loader_train:
            optimizer.zero_grad()
            output = model(data.to(device))

            # Select only the relevant nodes for the loss function
            idxmask = torch.where(data.mask)[0]
            mlabels = torch.index_select(data.y, 0, idxmask)
            moutput = torch.index_select(output, 0, idxmask)

            loss = criterion(moutput, mlabels)
            losses_train.append(loss.item())
            loss.backward()
            optimizer.step()
            pbar.update()

        # Use model on eval data
        model.eval()
        for data in loader_eval:
            with torch.no_grad():
                output = model(data.to(device))
                idxmask = torch.where(data.mask)[0]
                mlabels = torch.index_select(data.y, 0, idxmask)
                moutput = torch.index_select(output, 0, idxmask)

                val_loss = criterion(moutput, mlabels)
                losses_eval.append(val_loss.item())
                pbar.update()

        loss_train = np.mean(losses_train)
        loss_eval = np.mean(losses_eval)
        best_val_loss = min([loss_eval, best_val_loss])

        scheduler.step(loss_eval)
        adam_lr = optimizer.param_groups[0]['lr']

        postfix = {
            'training_loss': loss_train,
            'validation_loss': loss_eval,
            'precision': precision_score_eval,
            'recall': recall_score_eval,
            'adam_lr': adam_lr,
        }

        pbar.set_postfix(**{k: v for k, v in postfix.items() if v is not None})
        pbar.close()

        training_results.append({
            'epoch': epoch,
            **postfix
        })

        out_file = os.path.join(model_output_dir, f'model_epoch_{epoch:04d}.out')

        if args.keep_models > 1:

            delete_model = max(best_models.items(), key=lambda x: x[1],
                               default=None)  # returns filename, loss_eval for "worst" model

            if len(best_models) < args.keep_models:
                torch.save(model.state_dict(), out_file)
                best_models[out_file] = loss_eval

            elif delete_model and delete_model[1] > loss_eval:  # Save model
                torch.save(model.state_dict(), out_file)
                best_models[out_file] = loss_eval

                del best_models[delete_model[0]]
                try:  # delete model file
                    os.remove(delete_model[0])
                except OSError:
                    print(f'Failed to delete {delete_model[0]}.')
        else:
            torch.save(model.state_dict(), out_file)
            best_models[out_file] = loss_eval

        with open(f'{model_output_dir}/results.csv', 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, training_results[0].keys())
            dict_writer.writeheader()
            dict_writer.writerows(training_results)

        # save the best model
        best_model = min(best_models.items(), key=lambda x: x[1], default=None)
        if best_model:
            out_file = os.path.join(model_output_dir, f'best_model.out')
            torch.save(model.state_dict(), out_file)


def initialize_model(args):
    model = GGNN(**args.__dict__)

    if args.input_model:
        model.load_state_dict(
            torch.load(args.input_model, map_location=torch.device(args.device))
        )
    return model


def main():
    args = parse_args()

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    if args.dataset is None:
        print(f'Dataset not specified... exiting')
        return 1

    dataset = import_npz(args.dataset)

    args.num_features = dataset[0].x.shape[1]
    args.num_classes = dataset[0].y.shape[1]

    model = initialize_model(args)

    dir_name = '{}_{}_{}'.format(
        str(os.path.abspath(args.dataset)).replace('/', '_'),
        datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S"),
        str(uuid.uuid4())[:8]
    )

    full_dir_path = os.path.join(args.model_output_dir, dir_name)
    os.makedirs(full_dir_path, exist_ok=True)
    print(f'Writing output to {full_dir_path}')

    # directory exists and is empty
    if os.path.isdir(full_dir_path) and not os.listdir(full_dir_path):
        params = vars(args)
        params['dataset'] = os.path.abspath(params['dataset'])
        params['model_output_dir'] = os.path.abspath(full_dir_path)

        criterion = nn.MSELoss(reduction='mean')

        try:
            repo = git.Repo(search_parent_directories=True)
            current_hash = repo.head.object.hexsha
            params['git_commit'] = current_hash
        except:
            params['git_commit'] = None
        with open('{}/config.yml'.format(args.model_output_dir), 'w+') as f:
            yaml.dump(params, f)
    else:
        raise ValueError('Model directory either doesn\'t exist or is not empty')

    train_model(model, dataset, criterion, args)


def check_cuda(device):
    if device == 'cuda':
        try:
            torch.cuda.current_device()
            return 'cuda'
        except Exception as e:
            print(e)
            print('Fallback to cpu')
    return 'cpu'


def parse_config(config_file):
    print('Parsing parameters from config file.')

    with open(config_file, 'r') as f:
        model_config = yaml.safe_load(f)
    return model_config


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument("--seed", type=int, default=1, help="Seed used for random number generator")
    p.add_argument("--dataset", type=str, help="Directory of npz files")
    p.add_argument("--epochs", type=int, default=15, help="Number of epochs for training")
    p.add_argument("--learning-rate", type=float, default=5e-4, help="Learning rate for Adam")
    p.add_argument("--weight-decay", type=float, default=0, help="Weight decay rate for Adam")
    p.add_argument("--lr-scheduler-factor", type=float, default=5e-4, help="Learning rate for Adam")
    p.add_argument("--dropout", type=float, default=.5, help="Dropout used for between the linear layers")
    p.add_argument("--dropout-gru", type=float, default=.0, help="Dropout used between the GRUs")
    p.add_argument("--train-test-split", type=float, default=.75)
    p.add_argument("--batch-size", type=int, default=16, help="Batch size")
    p.add_argument("--hidden-size", type=int, default=64, help="Size of the hidden messages")
    p.add_argument("--nunroll", type=int, default=10, help="Number of loop unrolling for the Gated Graph NN")
    p.add_argument("--model-output-dir", type=str, default="/tmp", help="Output file of trained model parameters")
    p.add_argument("--input-model", type=str, help="Load model from file.")
    p.add_argument("--device", choices=['cpu', 'cuda'], help="Set the device", default='cuda')
    p.add_argument("--linear-layer-input", action="store_true", help="Set to use a linear layer before the GRU")
    p.add_argument("--num-layers", type=int, default=1, help="Number of GRU layers")
    p.add_argument("--keep-models", type=int, default=10,
                   help="Keep only the best N models during training. Values < 1 defaults to keep all models.")
    p.add_argument("--config", type=str,
                   help="Path to a config yaml file. Command line arguments overwrite parameters given in the config file.")
    args = p.parse_args()

    if args.config:
        config_args = parse_config(args.config)
        p.set_defaults(**config_args)
        args = p.parse_args()

    args.device = check_cuda(args.device)
    return args


if __name__ == "__main__":
    main()
