## Dataset

In the repository, you find the compressed raw data.
Use the following command in the train and validation dataset directory
to decompress the files.
```
tar -zxvf results.tar.gz
```

### Source Data
The results for each topology are stored in ``json`` files.
They also contain the original configuration which can be used as input for the simulation
using the following command, for example in the `dataset-train` directory:

```
for f in results/*;
do
    jq .input $f > input/$(basename $f);
done
```

This extracts the corresponding json fields using [`jq`](https://stedolan.github.io/jq/) and writes them in the `input`
directory.